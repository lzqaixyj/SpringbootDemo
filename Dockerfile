FROM java:8
EXPOSE 9090

VOLUME /tmp
ADD sms-send-service-webchinese.jar  /app.jar
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-jar","/app.jar"]
