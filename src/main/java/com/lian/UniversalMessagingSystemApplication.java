package com.lian;

import com.lian.common.aspect.RequestJsonHandlerMethodArgumentResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class UniversalMessagingSystemApplication extends WebMvcConfigurationSupport {

    public UniversalMessagingSystemApplication() {
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new RequestJsonHandlerMethodArgumentResolver());
        super.addArgumentResolvers(argumentResolvers);
    }

    public static void main(String[] args) {
        SpringApplication.run(UniversalMessagingSystemApplication.class, args);
    }

}
