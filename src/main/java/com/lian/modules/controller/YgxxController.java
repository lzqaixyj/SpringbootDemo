package com.lian.modules.controller;

import java.util.Arrays;
import java.util.Map;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.R;
import com.lian.modules.entity.YgxxEntity;
import com.lian.modules.service.YgxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 员工信息表
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@RestController
@RequestMapping("SecurityCheck/ygxx")
@CrossOrigin("*")
public class YgxxController {
    @Autowired
    private YgxxService ygxxService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = ygxxService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
		YgxxEntity ygxx = ygxxService.getById(id);

        return R.ok().put("ygxx", ygxx);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody YgxxEntity ygxx){
		ygxxService.save(ygxx);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody YgxxEntity ygxx){
		ygxxService.updateById(ygxx);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
		ygxxService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
