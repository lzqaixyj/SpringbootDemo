package com.lian.modules.controller;

import java.util.Arrays;
import java.util.Map;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.R;
import com.lian.modules.entity.YhddEntity;
import com.lian.modules.service.YhddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 隐患地点
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@RestController
@RequestMapping("SecurityCheck/yhdd")
@CrossOrigin("*")
public class YhddController {
    @Autowired
    private YhddService yhddService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = yhddService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
		YhddEntity yhdd = yhddService.getById(id);

        return R.ok().put("yhdd", yhdd);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody YhddEntity yhdd){
		yhddService.save(yhdd);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody YhddEntity yhdd){
		yhddService.updateById(yhdd);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
		yhddService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
