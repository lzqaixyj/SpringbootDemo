package com.lian.modules.controller;

import java.util.Arrays;
import java.util.Map;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.R;
import com.lian.modules.entity.YhxxEntity;
import com.lian.modules.service.YhxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 用户信息表
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@RestController
@RequestMapping("SecurityCheck/yhxx")
@CrossOrigin("*")
public class YhxxController {
    @Autowired
    private YhxxService yhxxService;

    /**
     * 登录
     */
    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public R login(String name,String password){
        YhxxEntity yhxxEntity = yhxxService.getUser(name,password);
        if (null==yhxxEntity){
            return R.error(1,"用户名密码错误");
        }
        return R.ok().put("page",yhxxEntity);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = yhxxService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
		YhxxEntity yhxx = yhxxService.getById(id);

        return R.ok().put("yhxx", yhxx);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody YhxxEntity yhxx){
		yhxxService.save(yhxx);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody YhxxEntity yhxx){
		yhxxService.updateById(yhxx);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
		yhxxService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
