package com.lian.modules.controller;

import java.util.Arrays;
import java.util.Map;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.R;
import com.lian.modules.entity.YhsjEntity;
import com.lian.modules.service.YhsjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 隐患数据表
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@RestController
@RequestMapping("SecurityCheck/yhsj")
@CrossOrigin("*")
public class YhsjController {
    @Autowired
    private YhsjService yhsjService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = yhsjService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
		YhsjEntity yhsj = yhsjService.getById(id);

        return R.ok().put("yhsj", yhsj);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody YhsjEntity yhsj){
		yhsjService.save(yhsj);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody YhsjEntity yhsj){
		yhsjService.updateById(yhsj);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
		yhsjService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
