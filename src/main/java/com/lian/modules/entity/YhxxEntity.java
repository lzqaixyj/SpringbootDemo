package com.lian.modules.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户信息表
 * 
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@Data
@TableName("yhxx")
public class YhxxEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 工号
	 */
	@TableId
	private String id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 职务
	 */
	private String job;
	/**
	 * 密码
	 */
	private String password;

}
