package com.lian.modules.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 隐患数据表
 * 
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@Data
@TableName("yhsj")
public class YhsjEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 排查时间
	 */
	private Date pctime;
	/**
	 * 隐患地点
	 */
	private String yhplace;
	/**
	 * 隐患内容
	 */
	private String content;
	/**
	 * 排查人
	 */
	private String pcren;
	/**
	 * 整改期限
	 */
	private Date deadline;
	/**
	 * 整改情况
	 */
	private String zgqk;
	/**
	 * 整改人
	 */
	private String zgren;
	/**
	 * 整改说明
	 */
	private String zgsm;
	/**
	 * 整改时间
	 */
	private Date zgtime;
	/**
	 * 复查人
	 */
	private String fcren;
	/**
	 * 复查意见
	 */
	private String fcyj;
	/**
	 * 复查时间
	 */
	private Date fctime;

}
