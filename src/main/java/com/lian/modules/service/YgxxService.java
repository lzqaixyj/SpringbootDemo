package com.lian.modules.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lian.common.utils.PageUtils;
import com.lian.modules.entity.YgxxEntity;

import java.util.Map;

/**
 * 员工信息表
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
public interface YgxxService extends IService<YgxxEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

