package com.lian.modules.service.impl;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.Query;
import com.lian.modules.dao.YhddDao;
import com.lian.modules.entity.YhddEntity;
import com.lian.modules.service.YhddService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("yhddService")
public class YhddServiceImpl extends ServiceImpl<YhddDao, YhddEntity> implements YhddService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<YhddEntity> page = this.page(
                new Query<YhddEntity>().getPage(params),
                new QueryWrapper<YhddEntity>()
        );

        return new PageUtils(page);
    }

}