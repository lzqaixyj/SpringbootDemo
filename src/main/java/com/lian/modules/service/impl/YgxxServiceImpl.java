package com.lian.modules.service.impl;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.Query;
import com.lian.modules.dao.YgxxDao;
import com.lian.modules.entity.YgxxEntity;
import com.lian.modules.service.YgxxService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("ygxxService")
public class YgxxServiceImpl extends ServiceImpl<YgxxDao, YgxxEntity> implements YgxxService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<YgxxEntity> page = this.page(
                new Query<YgxxEntity>().getPage(params),
                new QueryWrapper<YgxxEntity>()
        );

        return new PageUtils(page);
    }

}