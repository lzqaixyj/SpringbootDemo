package com.lian.modules.service.impl;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.Query;
import com.lian.modules.dao.YhxxDao;
import com.lian.modules.entity.YhxxEntity;
import com.lian.modules.service.YhxxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("yhxxService")
public class YhxxServiceImpl extends ServiceImpl<YhxxDao, YhxxEntity> implements YhxxService {
    @Autowired
    YhxxDao yhxxDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<YhxxEntity> page = this.page(
                new Query<YhxxEntity>().getPage(params),
                new QueryWrapper<YhxxEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public YhxxEntity getUser(String name, String password) {
        YhxxEntity yhxxEntity = yhxxDao.getUser(name,password);
        if (null==yhxxEntity){
            return null;
        }
        return yhxxEntity;
    }

}