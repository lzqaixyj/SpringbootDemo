package com.lian.modules.service.impl;

import com.lian.common.utils.PageUtils;
import com.lian.common.utils.Query;
import com.lian.modules.dao.YhsjDao;
import com.lian.modules.entity.YhsjEntity;
import com.lian.modules.service.YhsjService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("yhsjService")
public class YhsjServiceImpl extends ServiceImpl<YhsjDao, YhsjEntity> implements YhsjService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<YhsjEntity> page = this.page(
                new Query<YhsjEntity>().getPage(params),
                new QueryWrapper<YhsjEntity>()
        );

        return new PageUtils(page);
    }

}