package com.lian.modules.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lian.common.utils.PageUtils;
import com.lian.modules.entity.YhsjEntity;

import java.util.Map;

/**
 * 隐患数据表
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
public interface YhsjService extends IService<YhsjEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

