package com.lian.modules.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lian.common.utils.PageUtils;
import com.lian.modules.entity.YhxxEntity;

import java.util.Map;

/**
 * 用户信息表
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
public interface YhxxService extends IService<YhxxEntity> {

    PageUtils queryPage(Map<String, Object> params);

    YhxxEntity getUser(String name,String password);
}

