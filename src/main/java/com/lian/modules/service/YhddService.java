package com.lian.modules.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lian.common.utils.PageUtils;
import com.lian.modules.entity.YhddEntity;

import java.util.Map;

/**
 * 隐患地点
 *
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
public interface YhddService extends IService<YhddEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

