package com.lian.modules.dao;

import com.lian.modules.entity.YhxxEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 用户信息表
 * 
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@Mapper
public interface YhxxDao extends BaseMapper<YhxxEntity> {
	//登录dao
    public YhxxEntity getUser(@Param("name") String name, @Param("password") String password);
}
