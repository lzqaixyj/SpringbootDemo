package com.lian.modules.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lian.modules.entity.YgxxEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 员工信息表
 * 
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@Mapper
public interface YgxxDao extends BaseMapper<YgxxEntity> {
	
}
