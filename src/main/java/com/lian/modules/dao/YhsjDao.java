package com.lian.modules.dao;

import com.lian.modules.entity.YhsjEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 隐患数据表
 * 
 * @author dengzc
 * @email dengzc@gmail.com
 * @date 2020-05-13 20:00:41
 */
@Mapper
public interface YhsjDao extends BaseMapper<YhsjEntity> {
	
}
